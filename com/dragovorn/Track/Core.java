package com.dragovorn.Track;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Core {
	
	static List<String> remainingPlayers = new ArrayList<String>();
	
	static int numCaptains = 0;
	
	static Scanner cin = new Scanner(System.in);
	
	public static void main(String[] args) {
		String answer;
		
		do {
			System.out.println("Choose one:");
			System.out.println("A) Generate new roaster.");
			System.out.println("B) Read roaster.");
			System.out.println("C) Terminate");
			
			answer = cin.nextLine();
			
			System.out.print("Please enter the file's name: ");
			String fileName = cin.nextLine();
			
			if (answer.equalsIgnoreCase("a")) {
				
			} else if (answer.equalsIgnoreCase("b")) {
				parseFile(fileName);
			}
		} while (!answer.equalsIgnoreCase("c"));
		
		System.out.println("Goodbye...");
		
		cin.close();
	}
	
	public static void assignCaptains() {
		do {
			System.out.print("How many captains are you planing to have? ");
			numCaptains = cin.nextInt();
		} while (numCaptains > remainingPlayers.size());
		
		String captain = "";
		
		for (int x = 0; x < numCaptains; x++) {
			do {
				System.out.print("Please enter who you'd like to be a captain: ");
				captain = cin.nextLine();
			} while (!remainingPlayers.contains(captain));
			
			for (String str : remainingPlayers) {
				if (str.contains(captain) && !str.equalsIgnoreCase(captain)) {
					
				} else if (str.equalsIgnoreCase(captain)) {
					
				}
			}
		}
	}
	
	public static void parseFile(String fileName) {
		File file = new File("/track/" + fileName + ".rst");
		
		if (!file.exists()) {
			System.err.println(fileName + " does not exist in the \'track\' folder!");
			return;
		}
		
		try {
			FileReader fileReader = new FileReader(file);
			BufferedReader reader = new BufferedReader(fileReader);
			
			while (reader.readLine() != null) {
				remainingPlayers.add(reader.readLine());
			}
			
			reader.close();
		} catch (IOException exception) {
			exception.printStackTrace();
		}
	}
}